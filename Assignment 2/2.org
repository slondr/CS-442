#+title: CS 442 Assignment 2
#+subtitle: Queries: Relational Algebra + SQL
#+author: Eric S. Londres
#+options: toc:nil
#+latex_header: \usepackage[margin=1in]{geometry}


* 
# Question #1
#+caption: Hotel
|-------+---------+------------+---------|
| =hID= | =hName= | =hAddress= | =hCity= |
| /     | <       | <          | <       |
|-------+---------+------------+---------|

#+caption: Guest
|-------+---------+------------+---------|
| =gID= | =gName= | =gAddress= | =gCity= |
| /     | <       | <          | <       |
|-------+---------+------------+---------|

#+caption: Room
|--------+-------+----------+---------|
| =type= | =hID= | =roomNo= | =prize= |
| /      | <     | <        | <       |
|--------+-------+----------+---------|
#+begin_center
Table 4: Booking
#+end_center
|-------+-------+----------+------------+--------+------------|
| =gID= | =hID= | =roomNo= | =fromDate= | =year= | =noOfDays= |
| /     | <     | <        | <          | <      | <          |
|-------+-------+----------+------------+--------+------------|

- 1 ::
			 \rho(Records, Booking $\bowtie$ Guest $\bowtie$ Hotel)
			 
			 \rho(New-Yorkers, \sigma_{=hCity= = ``NYC''}(Records))

			 \Pi_{=gName=}(\sigma_{=fromDate= \geq ``Jan01, 2019'' \wedge =fromDate= \leq ``Jan31, 2019''}(New-Yorkers))
- 2 ::
			 \rho(New-Yorkers, \sigma_{=hCity= = ``NYC''}(Booking $\bowtie$ Guest $\bowtie$ Hotel) $\bowtie$ Room)

			 \rho(Suiters, \sigma_{=type= = ``Suite''}(New-Yorkers))

			 \Pi_{=gName=}\left(\sigma_{=noOfDays= > 10}(Suiters)\right)

- 3 ::
			 \rho(LessThanFive, \sigma_{=nOfDays= \leq 5} (Booking))

			 \Pi_{=gName=}\(\left(\mathrm{Guest} \bowtie \mathrm{LessThanFive}\right)\)
- 4 ::
			 \rho(S, Bookings - \sigma_{=year= = 2018}(Bookings))
			 
			 \Pi_{=hID=}(S)
- 5 ::
			 \rho(NewYorkHotels, \sigma_{=hCity= = ``NYC''}(Hotel))

			 \rho(NewYorkSuites, \sigma_{=type= = ``suite''}(Room $\bowtie$ NewYorkHotels))

			 \Pi_{=gID=}(Booking / NewYorkSuites)
			 
# Question #2
* 
Schema:
| / | <      |       | >       |
|---+--------+-------+---------|
|   |        | *Emp* |         |
|   | _eid_  |       | integer |
|   | ename  |       | string  |
|   | age    |       | integer |
|   | salary |       | real    |
|---+--------+-------+---------|

| / | <     |         | >       |
|---+-------+---------+---------|
|   |       | *Works* |         |
|   | _eid_ |         | integer |
|   | _did_ |         | integer |
|---+-------+---------+---------|

| / | <         |        | >       |
|---+-----------+--------+---------|
|   |           | *Dept* |         |
|   | _did_     |        | integer |
|   | budget    |        | real    |
|   | managerid |        | integer |
|   | dname     |        | string  |
|---+-----------+--------+---------|
- 1 :: The name and age of all employees who work in both Hardware and Software:
			 #+begin_src sql
				 SELECT ename, age
				 FROM Emp
				 WHERE eid=ANY (
						SELECT A.eid
						FROM Works A, Works B, Dept
						WHERE A.eid = B.eid AND A.did=ANY (
									SELECT did
									FROM Dept
									WHERE dname='Hardware'
						) AND B.did = ANY (
									SELECT did
									FROM Dept
									WHERE dname='Software'
						)
			 #+end_src
- 2 :: The id of the manager who controls the largest total amount of budget:
#+begin_src sql
	SELECT managerid
	FROM Dept
	WHERE budget = (
	  SELECT MAX d.budget
		FROM Dept d
		WHERE d.budget = (
		  SELECT SUM d2.budget
			FROM Dept d2, Dept d3
			WHERE d2.managerid = d3.managerid))
#+end_src
- 3 :: The id of managers whose department's budget is over $1 million:
#+begin_src sql
	SELECT managerid
	FROM Dept
	WHERE budget > 1000000 AND managerid NOT IN (
		 SELECT managerid
		 FROM Dept
		 WHERE budget < 1000000
	)
#+end_src
- 4 :: Names of all employees whose salary exceeds the budget of all the departments that the employee works in:
#+begin_src sql
	SELECT ename, eid AS selected_id
	FROM Emp e
	WHERE salary > (
		SELECT MAX (DISTINCT budget)
		FROM Dept
		WHERE did IN (
			SELECT DISTINCT did
			FROM Works w
			WHERE w.eid = selected_id
		)
	)
#+end_src
- 5 :: Names of managers who manage departments with the largest budgets:
#+begin_src sql
	SELECT ename
	FROM Emp
	WHERE eid IN (
		SELECT managerid
		FROM Dept
		WHERE budget = (
			SELECT MAX (DISTINCT budget)
			FROM Dept
		)
	)
#+end_src
* 
# Question #3
- 1 ::
	+ a :: Address, house ID, asking price, and selling price of houses which sold for less than their asking price:


				\Pi_{(=address=, =houseID=, =asking_price=, =selling_price=)}(\sigma_{=asking_price= > =selling_price=}(House $\bowtie$ Sold))


	+ b :: Names of buyers who haven't bought a house:
				
				\rho(Bought, Buyer \bowtie_{(id = buyerID)} Sold)

				\rho(Unbought, Buyer - Bought)

				\Pi_{name}(Unbought)

- 2 ::
	+ a :: For each postal code in which there were at least three houses sold, find the postal code and the  average selling price of houses in that postal code.
#+begin_src sql
	SELECT sh.postal_code, avgprice
	FROM (House INNER JOIN Sold ON House.id = Sold.houseID) AS sh
	WHERE (sh.postal_code = (
		SELECT COUNT(postal_code)
		FROM sh
	)) > 3, avgprice = (
		SELECT AVG(selling_price)
		FROM sh
		WHERE postal_code = sh.postal_code
	)
#+end_src
  + b :: Find the addresses and asking prices of all houses that have at least 4 bedrooms and 2 bathrooms that have not been sold. Each (address, asking price) pair should appear only once.
#+begin_src sql
	SELECT DISTINCT address, asking_price
	FROM House h, (House INNER JOIN Sold ON House.id = Sold.houseID) AS sh
	WHERE h.id NOT IN sh
		AND h.baths >= 2
		AND h.beds >= 4
#+end_src

I pledge my honor that I have abided by the Stevens Honor System.

---Eric S. Londres
