#+options: toc:nil

\setlength{\parindent}{0pt}
* 
Candidate Key: =CDF=
* 
$F \rightarrow AD \implies F \rightarrow A$

$F \rightarrow AD \land AD \rightarrow B \implies F \rightarrow B$

$F \rightarrow A \land F \rightarrow B \implies F \rightarrow AB$;
$F \rightarrow AB \land AB \rightarrow C \implies F \rightarrow C$

$F \rightarrow AD \implies F \rightarrow D$

$F \rightarrow E$

$F^+ = \lbrace A, B, C, D, E, F\rbrace$, so $F$ is a superkey.
* 
This decomposition does /not/ satisfy BCNF. =StudCourse= must be further decomposed into the following relations:

=Course(CourseNo, Ctitle, Instrucname)=

=StudGrade(StudNo, CourseNo, Grade)=

=Instruc(Instrucname, InstrucLocn)=

Also, =StudMajor= must be decomposed into the following:

=Advisors(Advisor, Major)=

=StudAdvisor(StudNo, Advisor)=

Now the decomposition satisfies BCNF.
