'use strict';

const mysql = require('mysql');
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'password'
});

async function lab() {
    
    connection.connect();
    console.log('Connected to the database server.');
    
    // remove the database if it has already been created
    await connection.query('DROP DATABASE IF EXISTS VehicleOffice');
    // create the databaseg
    await connection.query('CREATE DATABASE VehicleOffice');
    console.log('The database has been created.');
    await connection.query('USE VehicleOffice');
    
    // table statements
    const create_branch = `
CREATE TABLE branch(
  branch_id INTEGER NOT NULL PRIMARY KEY,
  branch_name VARCHAR(20) NOT NULL,
  branch_addr VARCHAR(50) NOT NULL,
  branch_city VARCHAR(20),
  branch_phone INTEGER
)
`;
    
    const create_driver = `
CREATE TABLE driver(
  driver_ssn INTEGER NOT NULL PRIMARY KEY,
  driver_name VARCHAR(20) NOT NULL,
  driver_addr VARCHAR(50) NOT NULL,
  driver_city VARCHAR(20) NOT NULL,
  driver_birthdate DATE NOT NULL,
  driver_phone INTEGER
)
`;

    /// Create Table License
    const create_license = `
CREATE TABLE license(
  license_no INTEGER NOT NULL PRIMARY KEY,
  driver_ssn INTEGER NOT NULL,
  license_type CHAR NOT NULL,
  license_class INTEGER NOT NULL,
  license_expiry DATE NOT NULL,
  issue_date DATE NOT NULL,
  branch_id INTEGER NOT NULL
)`;
    /// TODO: Create Table Exam
    const create_exam = `
CREATE TABLE exam(
  driver_ssn INTEGER NOT NULL,
  branch_id INTEGER NOT NULL,
  exam_date DATE NOT NULL,
  exam_type CHAR NOT NULL,
  exam_score INTEGER,
  PRIMARY KEY(driver_ssn, branch_id, exam_date)
)`;
    

    
    await connection.query(create_branch);
    await connection.query(create_driver);
    await connection.query(create_license);
    await connection.query(create_exam);
    console.log('The tables have been created.');

    // Insert data into branch
    await connection.query("INSERT INTO branch VALUES(10, 'Main', '1234 Main St.', 'Hoboken', 5551234)");
    await connection.query("INSERT INTO branch VALUES(20, 'NYC', '23 No.3 Road', 'NYC', 5552331)");
    await connection.query("INSERT INTO branch VALUES(30, 'West Creek', '251 Creek Rd.', 'Newark', 5552511)");
    await connection.query("INSERT INTO branch VALUES(40, 'Blenheim', '1342 W.22 Ave.', 'Princeton', 5551342)");

    // Insert data into driver
    await connection.query("INSERT INTO driver VALUES(11111111, 'Bob Smith', '111 E. 11 St.', 'Hoboken', '1975-01-01', 5551111)");
    await connection.query("INSERT INTO driver VALUES(22222222, 'John Walters', '222 E. 22 St.', 'Princeton', '1976-02-02', 5552222)");
    await connection.query("INSERT INTO driver VALUES(33333333, 'Troy Rops', '333 W.33 Ave.', 'NYC', '1970-03-03', 5553333)");
    await connection.query("INSERT INTO driver VALUES(44444444, 'Kevin Mark', '444 E.4 Ave.', 'Hoboken', '1974-04-04', 5554444)");

    ///  Task 5 (insert into license)
    await connection.query("INSERT INTO license VALUES(1, 11111111, 'D', 5, '2022-05-24', '2017-05-25', 20)");
    await connection.query("INSERT INTO license VALUES(2, 22222222, 'D', 5, '2023-08-29', '2016-08-29', 40)");
    await connection.query("INSERT INTO license VALUES(3, 33333333, 'L', 5, '2022-12-27', '2017-06-27', 20)");
    await connection.query("INSERT INTO license VALUES(4, 44444444, 'D', 5, '2022-08-30', '2017-08-30', 40)");

    /// Task 6 (insert into exam)
    await connection.query("INSERT INTO exam VALUES(11111111, 20, '2017-05-25', 'D', 79)");
    await connection.query("INSERT INTO exam VALUES(11111111, 20, '2017-12-02', 'L', 67)");
    await connection.query("INSERT INTO exam VALUES(22222222, 30, '2016-05-06', 'L', 25)");
    await connection.query("INSERT INTO exam VALUES(22222222, 40, '2016-06-10', 'L', 51)");
    await connection.query("INSERT INTO exam VALUES(22222222, 40, '2016-08-29', 'D', 81)");
    await connection.query("INSERT INTO exam VALUES(33333333, 10, '2017-07-07', 'L', 45)");
    await connection.query("INSERT INTO exam VALUES(33333333, 20, '2017-06-27', 'L', 49)");
    await connection.query("INSERT INTO exam VALUES(33333333, 20, '2017-07-27', 'L', 61)");
    await connection.query("INSERT INTO exam VALUES(44444444, 10, '2017-07-27', 'L', 71)");
    await connection.query("INSERT INTO exam VALUES(44444444, 20, '2017-08-30', 'L', 65)");
    await connection.query("INSERT INTO exam VALUES(44444444, 40, '2017-09-01', 'L', 62)");

    console.log('The tables have been populated with data.\n');

    
    /// FOUR QUERIES

    // Query 1
    const query1 = `
SELECT driver_name
FROM driver
WHERE driver_ssn IN (
  SELECT driver_ssn
  FROM license
  WHERE branch_id IN (
    SELECT branch_id
    FROM branch
    WHERE branch_city = "NYC"
))`;
    connection.query(query1, (err, ret, fields) => {
	if(err) throw err;
	console.log('Names of drivers who got their license in New York City:');
	ret.forEach(row => console.log(row.driver_name));
	console.log(); // print newline
    });

    // Query 2
    const query2 = "SELECT driver_name FROM driver WHERE driver_ssn IN (SELECT driver_ssn FROM license WHERE license_expiry < '2022-12-31')";
    connection.query(query2, (err, ret, fields) => {
	if(err) throw err;
	console.log('Names of drivers whose license expires by December 31st, 2022:');
	ret.forEach(row => console.log(row.driver_name));
	console.log(); // print newline
    });

    // Query 3
    // "name of the drivers who took at least 2 exams for the same driver license type at the same branch"
    const query3 = `
SELECT driver_name
FROM driver
WHERE driver_ssn IN (
  SELECT driver_ssn
  FROM exam
  GROUP BY driver_ssn, branch_id, exam_type
  HAVING COUNT(*) > 1
)
`;
    connection.query(query3, (err, ret, fields) => {
	if(err) throw err;
	console.log('Names of drivers who took at least 2 exams for the same license type at the same branch:');
	ret.forEach(row => console.log(row.driver_name));
	console.log(); // print newline
    });

    // Query 4
    // "The name of the drivers whose exam scores get /consecutively/ lower when he/she took more exams
    const query4 = `
SELECT driver_name
FROM driver
WHERE driver_ssn IN (
  SELECT e0.driver_ssn
  FROM (
    SELECT driver_ssn, COUNT(driver_ssn) AS adj_count
    FROM (
      SELECT e1.driver_ssn, e1.exam_score
      FROM exam e1, exam e2
      WHERE e1.exam_date > e2.exam_date AND e1.exam_score < e2.exam_score AND e1.driver_ssn = e2.driver_ssn AND e1.exam_type = e2.exam_type
      GROUP BY e1.driver_ssn, e1.exam_score
    ) e
    GROUP BY driver_ssn
  ) e2 INNER JOIN (
    SELECT driver_ssn, COUNT(driver_ssn) AS raw_count
    FROM exam
    GROUP BY driver_ssn
  ) e0 ON e0.driver_ssn = e2.driver_ssn
  WHERE raw_count - adj_count = 1
)
`;
    connection.query(query4, (err, ret, fields) => {
	if(err) throw err;
	console.log('Names of drivers whose scores got consecutively worse:');
	ret.forEach(row => console.log(row.driver_name));
	console.log(); // print newline
    });

    connection.end();
}

lab();
