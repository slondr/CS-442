#+TITLE: CS 442 ER Diagram & Relational Model
#+AUTHOR: Eric S. Londres
#+DATE: 27 September 2019
#+OPTIONS: toc:nil
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}

* 
- (a) :: 
   [[./1(a).png]]
   The candidate keys are =ssn= and =(id, semester)=.
- (b) :: 
   [[./1(b).png]]
   The candidate keys are =ssn= and =id=.
- (c) ::
	 [[./1(c).png]]
- (d) ::
	 [[./1(d).png]]

* 
- (a) ::
	 [[./2(a).png]]

- (b) ::
#+BEGIN_SRC sql
CREATE TABLE Assignment (
       assignedpatient int UNIQUE FOREIGN KEY REFERENCES Patients(number),
       roomnumber int FOREIGN KEY REFERENCES Rooms(number)
)

CREATE TABLE Treatment (
       assignedpatient int FOREIGN KEY REFERENCES Patients(number),
       treatingdoc int FOREIGN KEY REFERENCES Doctors(id)
)
#+END_SRC
