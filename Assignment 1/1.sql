CREATE TABLE Assignment (
       assignedpatient int UNIQUE FOREIGN KEY REFERENCES Patients(number),
       roomnumber int FOREIGN KEY REFERENCES Rooms(number)
)

CREATE TABLE Treatment (
       assignedpatient int FOREIGN KEY REFERENCES Patients(number),
       treatingdoc int FOREIGN KEY REFERENCES Doctors(id)
)
